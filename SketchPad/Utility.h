#pragma once
#include "stdafx.h"

class Utility
{
	public:
		static bool FileDialog(HWND &hWnd, char *fileString, char *title, bool saving);
		static void	SaveBitmap(char *fileString, HBITMAP &hBitmap, const long &imageWidth, const long &imageHeight);
		static COLORREF ColourDialog(const COLORREF colour, HWND &hWnd);
		static void	ColourButton(const COLORREF colour, HWND &button);

};