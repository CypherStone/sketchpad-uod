#include "Scanline.h"
#include "stdafx.h"

Scanline::Scanline(void)
{

}

Scanline::Scanline(const std::vector<Vertex> vertex)
{
	_yMax = (int)vertex[0].GetY();
	_xMax = (int)vertex[0].GetX();
	_yMin = _yMax;
	_xMin = _xMax;

	for(int i = 0; i < vertex.size(); i++)
	{
		if((int)vertex[i].GetY() > _yMax)
			_yMax = (int)vertex[i].GetY();
		else if((int)vertex[i].GetY() < _yMin)
			_yMin = (int)vertex[i].GetY();

		if((int)vertex[i].GetX() > _xMax)
			_xMax = (int)vertex[i].GetX();
		else if((int)vertex[i].GetX() < _xMin)
			_xMin = (int)vertex[i].GetX();
	}

	for (int i = 0; i <= (int)_yMax -_yMin; i++)
	{
		_scanline.push_back(std::vector<int>());
	}
}

Scanline::Scanline(const Scanline &scan)
{
	Copy(scan);
}

Scanline::~Scanline(void)
{
}

void Scanline::AddScanline(int y, int x)
{
	// A simple for loop that does not allow duplicate x values.
	for(int xVal = 0; xVal < _scanline.at(y).size(); xVal++)
	{
		if(x == _scanline.at(y).at(xVal))
			return;
	}
	_scanline.at(y).push_back(x);
}

int Scanline::GetScanline(int y, int x)
{
	return _scanline[y][x];
}

int Scanline::GetScanlineMinY(void)
{
	return _yMin;
}

int Scanline::GetScanlineMinX(void)
{
	return _xMin;
}

int Scanline::GetScanlineMaxY(void)
{
	return _yMax;
}

int Scanline::GetScanlineMaxX(void)
{
	return _xMax;
}

void Scanline::Sort(void)
{
	for(int position = 0; position < _scanline.size(); position++)
		std::sort(_scanline.at(position).begin(), _scanline.at(position).end());
}

void Scanline::Copy(const Scanline &scan)
{
	_scanline = scan._scanline;
	_yMax = scan._yMax;
	_xMax = scan._xMax;
	_xMin = scan._xMin;
	_yMin = scan._yMin;
}