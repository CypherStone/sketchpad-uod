#pragma once
#include "stdafx.h"
#include "Scanline.h"

class Drawing
{
	private:
		static void IncrementEvaluate(const long &a, const long &b, float &incrementA, float &incrementB);
		static void DrawEllipsesPoints(HDC &hdc, COLORREF colour, int outlineSize,long xStart, long x, long yStart, long y);
		static void SizeEvaluate(long &a, long &b);	

	public:
		static void DrawPoint(HDC &hdc, class Vertex, COLORREF, int);
		static void DrawLine(HDC &hdc, long x1, long y1, long x2, long y2, COLORREF, int, class Scanline &scanline, bool lines);
		static void DrawEllipses(HDC &hdc, RECT, bool,COLORREF, COLORREF, int);
		static void DrawRectangles(HDC &hdc, long, long, long, long, bool, COLORREF, COLORREF, int);
		static void FloodFill(HDC &hdc, long x, long y, const COLORREF colour, const COLORREF oldColour, int outlineSize);
		static void ScanlineFill(HDC &hdc, class Scanline &scanline ,COLORREF);
		static void DrawPolygons(HDC &hdc, std::vector<Vertex> &vertices, bool, COLORREF, COLORREF, int, bool finsihShape);
		static void DrawStar(HDC &hdc, long x1, long y1, long x2, long y2, int points, bool filled, COLORREF outline, COLORREF fill, int outlinePixels);
};