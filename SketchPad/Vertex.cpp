#include "Vertex.h"
#include "stdafx.h"
// Constructors

Vertex::Vertex(void)
{
	Init(0, 0);
}

Vertex::Vertex(long x, long y)
{
	Init(x, y);
}

Vertex::Vertex(const Vertex& v)
{
	Copy(v);
}

Vertex::~Vertex(void)
{
}

// Accessors and mutators

long Vertex::GetX(void) const
{
	return _x;
}

void Vertex::SetX(long x)
{
	_x = x;
}

long Vertex::GetY(void) const
{
	return _y;
}

void Vertex::SetY(long y)
{
	_y = y;
}

// Operator overloads

Vertex& Vertex::operator= (const Vertex &rhs)
{
	if (this != &rhs)
	{
		// Only copy if we are not assigning to ourselves. 
		// (remember that a = b is the same as a.operator=(b))
		Copy(rhs);
	}
	return *this;
}

const Vertex Vertex::operator+(const Vertex &other) const
{
	Vertex result;
	result.SetX(_x + other.GetX());
	result.SetY(_y + other.GetY());
	return result;
}

const Vertex Vertex::operator-(const Vertex &other) const
{
	Vertex result;
	result.SetX(_x - other.GetX());
	result.SetY(_y - other.GetY());
	return result;
}


// Private methods

void Vertex::Init(long x, long y)
{
	_x = x;
	_y = y;
}

void Vertex::Copy(const Vertex& v)
{
	_x = v.GetX();
	_y = v.GetY();
}




