﻿#include "Drawing.h"
#include "stdafx.h"

void Drawing::SizeEvaluate(long &a, long &b)
{
	if(a < b)
	{
		long temp = b;
		b = a;
		a = temp;
	}
}

void Drawing::IncrementEvaluate(const long &a, const long &b, float &incrementA, float &incrementB)
{
	if (a >= b) 
	{ 
		incrementA = -1; 
		incrementB = -1; 
	}
	else
	{
		incrementA = 1; 
		incrementB = 1; 
	}
}

void Drawing::DrawPoint(HDC &hdc, Vertex vertex, COLORREF colour, int size)
{
	// Draws a point depending on the line width size.
	if(size <= 1)
		SetPixelV(hdc, vertex.GetX() ,vertex.GetY(), colour);
	else
		DrawRectangles(hdc, vertex.GetX(), vertex.GetY(), vertex.GetX()+size, vertex.GetY()+size, true, colour, colour, 1);
}

void Drawing::DrawLine(HDC &hdc, long x1, long y1, long x2, long y2, COLORREF color, int size, Scanline &scanline, bool lines)
{	 
	// Based on the Bresenham's line drawing algorithm.
	
	float deltaX = (float)abs(x2 - x1);
	// It's a vertical line.
	if (deltaX == 0 && !lines) 
	{
		SizeEvaluate(y2, y1);
		HBRUSH brush = CreateSolidBrush(color);
		HGDIOBJ oldBrush = SelectObject(hdc, brush);
		BitBlt(hdc, x1, y1, size, y2 - y1, hdc, 0, 0, PATCOPY);
		SelectObject(hdc, oldBrush);
		DeleteObject(brush);	
		DeleteObject(oldBrush);
		return;
	}

	float deltaY = (float)abs(y2 - y1);
	// It's a horizontal line.
	if (deltaY == 0 && !lines) 
	{ 
		SizeEvaluate(x2, x1);
		HBRUSH brush = CreateSolidBrush(color);
		HGDIOBJ oldBrush = SelectObject(hdc, brush);
		BitBlt(hdc, x1, y1, x2 - x1, size, hdc, 0, 0, PATCOPY);
		SelectObject(hdc, oldBrush);
		DeleteObject(brush);	
		DeleteObject(oldBrush);
		return;
	}

	float xIncrementA, xIncrementB, yIncrementA, yIncrementB, denominator, numerator, numeratorAdd, pixelCount;
	IncrementEvaluate(x1, x2, xIncrementA, xIncrementB);
	IncrementEvaluate(y1, y2, yIncrementA, yIncrementB);

	if (deltaX >= deltaY)  
	{  
		xIncrementA = 0;   
		yIncrementB = 0;                     
		denominator = deltaX;
		numerator = deltaX / 2;
		numeratorAdd = deltaY;
		pixelCount = deltaX;        
	}
	else                             
	{
		xIncrementB = 0;                        
		yIncrementA = 0;    
		denominator = deltaY;
		numerator = deltaY / 2;
		numeratorAdd = deltaX;
		pixelCount = deltaY;    
	}
	
	// It's a diagonal line.
	float x = (float)x1, y = (float)y1;
	for (float currentPixel = 0; currentPixel <= pixelCount; currentPixel++)
	{
		if(lines)
			scanline.AddScanline(scanline.GetScanlineMaxY() - (int)y, (int)x);

		DrawPoint(hdc, Vertex((long)x, (long)y), color, size);          
		numerator += numeratorAdd;   

		if (numerator >= denominator)           
		{
			numerator -= denominator;            
			x += xIncrementA;                 
			y += yIncrementA;            
		}

		x += xIncrementB;                       
		y += yIncrementB;  
	}
}

void Drawing::DrawEllipsesPoints(HDC &hdc, COLORREF colour, int outlineSize,long xStart, long x, long yStart, long y)
{
	// Draws the required ellipses points.
	long modifier = 0;
	if(outlineSize > 1)
		modifier = outlineSize;

	Vertex v(xStart + x, yStart + y);
	DrawPoint(hdc, v, colour, outlineSize);
	v = Vertex(xStart - x - modifier, yStart + y);
	DrawPoint(hdc, v, colour, outlineSize);
	v = Vertex(xStart + x, yStart - y - modifier);
	DrawPoint(hdc, v, colour, outlineSize);
	v = Vertex(xStart - x - modifier, yStart - y - modifier);
	DrawPoint(hdc, v, colour, outlineSize);
}

void Drawing::DrawEllipses(HDC &hdc, RECT rect, bool filled,COLORREF colourOutline, COLORREF colourFill, int outlineSize)
{
	// Based on the Bresenham's circle drawing algorithm.
	// Adjusting calulations for better shape drawing compatabiltiy. 
	long tempVal = rect.bottom;
	long tempVal2 = rect.top;
	SizeEvaluate(tempVal, tempVal2);
	rect.bottom = tempVal - tempVal2;
	tempVal = rect.right;
	tempVal2 = rect.left;
	SizeEvaluate(tempVal, tempVal2);
	rect.right = tempVal - tempVal2;

	long width = rect.right * rect.right;
    long height = rect.bottom * rect.bottom;
	
	// Makes sure it always draws an ellipses.
	if(height < 1)
	{
		DrawLine(hdc, rect.left - rect.right,rect.top,rect.right + rect.left + outlineSize,rect.top,colourOutline,outlineSize, Scanline(), false);
		return;
	}
	else if(width < 1)
	{
		DrawLine(hdc, rect.left,rect.bottom + rect.top + outlineSize,rect.left,rect.top - rect.bottom ,colourOutline,outlineSize, Scanline(), false);
		return;
	}

    long maxWidth = 4 * width;
	long maxHeight = 4 * height;
	long xStart = rect.left;
	long yStart = rect.top;
	long long x, y,sigma;

	if(rect.right > 900)
		int i = 0;

    for (x = 0, y = rect.bottom, sigma = 2 * height + width * (1 - 2 * rect.bottom); height * x <= width * y; x++)
    {
		if(filled)
		{
			DrawLine(hdc, xStart - x, yStart + y - 1, xStart + x + 1, yStart + y - 1, colourFill, 1,Scanline(),false);
			DrawLine(hdc, xStart - x , yStart - y + 1, xStart + x + 1, yStart - y + 1, colourFill, 1,Scanline(),false);
		}

		DrawEllipsesPoints(hdc, colourOutline, outlineSize, xStart, x, yStart, y);

        if (sigma > 0)
        {
            sigma += maxWidth * (1 - y);
			y--;
        }
        sigma += height * ((4 * x) + 6);
    }

	for (x = rect.right, y = 0, sigma = 2 * width + height * (1 - 2 * rect.right); width * y <= height * x; y++)
    {
		if(filled)
		{
			DrawLine(hdc, xStart - x, yStart + y, x + xStart, yStart + y, colourFill, 1,Scanline(),false);
			DrawLine(hdc, xStart - x, yStart - y, x + xStart, yStart - y, colourFill, 1,Scanline(),false);
		}

		DrawEllipsesPoints(hdc, colourOutline, outlineSize, xStart, x, yStart, y);

        if (sigma >= 0)
        {
            sigma += maxHeight * (1 - x);
			x--;
        }
        sigma += width * ((4 * y) + 6);
    }
}

void Drawing::DrawRectangles(HDC &hdc, long x1, long y1, long x2, long y2, bool filled, COLORREF colour, COLORREF fillColour, int outlineSize)
{
	// By using the DrawLine function, I can create a rectangle.
	SizeEvaluate(y2, y1);
	SizeEvaluate(x2, x1);
	DrawLine(hdc, x1, y1, x2, y1, colour, outlineSize, Scanline(), false);
	DrawLine(hdc, x2, y1, x2, y2, colour, outlineSize, Scanline(), false);
	DrawLine(hdc, x1, y1, x1, y2 + outlineSize, colour, outlineSize, Scanline(), false);
	DrawLine(hdc, x1, y2, x2 + outlineSize, y2, colour, outlineSize, Scanline(), false);
	
	if(filled && (y2 - y1 - outlineSize> 0 && x2 - x1 - outlineSize > 0))
	{			
		HBRUSH brush = CreateSolidBrush(fillColour);
		HGDIOBJ oldBrush = SelectObject(hdc, brush);
		BitBlt(hdc, x1 + outlineSize, y1 + outlineSize, (x2 - outlineSize) - (x1), (y2 - outlineSize) - (y1), hdc, 0, 0, PATCOPY);
		SelectObject(hdc, oldBrush);
		DeleteObject(brush);	
		DeleteObject(oldBrush);
	}
}

void Drawing::FloodFill(HDC &hdc, long x1, long y1, const COLORREF colour, const COLORREF oldColour, int outlineSize)
{
	// The standard floodfill algorithm, using the queue method.

    if(colour == oldColour) 
	{
		//avoids infinite loops and filling the same colour.
		return; 
	}

	std::vector<Vertex> queue;
	queue.push_back(Vertex(x1, y1));

	for(int i = 0; i < (int)queue.size(); i++)
	{
		Vertex element = queue[i];
		int x = element.GetX();
		int y = element.GetY();

		if(GetPixel(hdc, x, y) == oldColour)
		{
			int leftToRight = 0;
			Vertex left = element;
			Vertex right = element;

			left.SetX(left.GetX() - 1); 
			right.SetX(right.GetX() + 1); 

			std::vector<Vertex>leftQueue;
			std::vector<Vertex>rightQueue;

			while (GetPixel(hdc, left.GetX(), y) == oldColour)
			{
				long value = left.GetX();
				left.SetX(value - 1);
				leftQueue.push_back(left);
			}

			while (GetPixel(hdc, right.GetX(), y) == oldColour)
			{
				long value = right.GetX();
				right.SetX(value + 1);
				rightQueue.push_back(right);
			}

			DrawLine(hdc, left.GetX() + 1, y, right.GetX(), y, colour, 1, Scanline(), false);
			leftQueue.insert(leftQueue.end(), rightQueue.begin(), rightQueue.end());
			
			int queueSize = (int)leftQueue.size();
			int editValues = 0;
			if(outlineSize < 2)
			{
				queueSize -= 1;
				editValues += 1;
			}

			for (int queuePosition = 1; queuePosition < queueSize; queuePosition++)
			{
				long tempX = leftQueue[queuePosition].GetX();
				COLORREF up = GetPixel(hdc, tempX + editValues, y + 1);
				COLORREF down = GetPixel(hdc, tempX + editValues, y - 1);

				if(up == oldColour)
					queue.push_back(Vertex(tempX, y + 1));

				if(down == oldColour)
					queue.push_back(Vertex(tempX, y - 1));
			}


		}
	}
}

void Drawing::ScanlineFill(HDC &hdc, Scanline &scanline, COLORREF colour)
{
	// A basic Scanline Fill algorithm, which doesn't handle complex shape too well.
	HBRUSH brush = CreateSolidBrush(colour);
	HGDIOBJ oldBrush = SelectObject(hdc, brush);
    int y = scanline.GetScanlineMinY() + 1;
	int yMax = scanline.GetScanlineMaxY();
	scanline.Sort();

	while (y < yMax)
	{	
		int x = scanline.GetScanlineMinX();
		int xMax = scanline.GetScanlineMaxX();
		int ySum = yMax - y;
		bool edge = false;
		int xPoint = 0;

		while (x <= xMax)
		{		
			int currentScanlineYSize = scanline._scanline[ySum].size();
			
			for(int i = 0; i < currentScanlineYSize; i++)
			{	
				int currentScanlineX = scanline.GetScanline(ySum, i);
				bool currentEdge = x == currentScanlineX;

				if (currentEdge && !edge)
				{
					if(i + 1 < currentScanlineYSize)
					{
						if (x + 1 != scanline.GetScanline(ySum, i + 1))
						{
							edge = true;
							xPoint = x;
						}
					}
				}
				else if(currentEdge && edge) // Edge is ready to be filled.
				{
					// Using BitBlt to quickly fill a single line.
					BitBlt(hdc, xPoint + 1, y, x - xPoint - 1, 1, hdc, 0, 0, PATCOPY);
					edge = false;
					xPoint = 0;
				}
			}
			x++;
		}
		y++;
	}
	SelectObject(hdc, oldBrush);
	DeleteObject(brush);	
	DeleteObject(oldBrush);
}

void Drawing::DrawPolygons(HDC &hdc, std::vector<Vertex> &vertices, bool filled, COLORREF colourOutline, COLORREF colourFill, int outlineSize, bool finishShape)
{
	// My basic line drawing algorithm.
	Scanline scanline(vertices);

	for(int i = 0; i < (int) vertices.size(); i++)
	{
		if(i + 1 < (int) vertices.size())
			DrawLine(hdc, vertices.at(i).GetX(), vertices.at(i).GetY(), vertices.at(i + 1).GetX(), vertices.at(i + 1).GetY(), colourOutline, outlineSize, scanline, filled);
		else if(finishShape)
			DrawLine(hdc, vertices.at(i).GetX(), vertices.at(i).GetY(), vertices.at(0).GetX(), vertices.at(0).GetY(), colourOutline, outlineSize, scanline, filled);
	}

	if(filled && finishShape)
	{
		ScanlineFill(hdc, scanline, colourFill);
		// A simple re-draw of the lines to stop the scanline overlapping if the line width is > 1.
		for(int i = 0; i < (int) vertices.size(); i++)
		{
			if(i + 1 < (int) vertices.size())
				DrawLine(hdc, vertices.at(i).GetX(), vertices.at(i).GetY(), vertices.at(i + 1).GetX(), vertices.at(i + 1).GetY(), colourOutline, outlineSize, scanline, false);
			else if(finishShape)
				DrawLine(hdc, vertices.at(i).GetX(), vertices.at(i).GetY(), vertices.at(0).GetX(), vertices.at(0).GetY(), colourOutline, outlineSize, scanline, false);
		}
	}
}

void Drawing::DrawStar(HDC &hdc, long x1, long y1, long x2, long y2, int points, bool filled, COLORREF outline, COLORREF fill, int outlinePixels)
{
	// Creates a circle of different point size, which then will get turned into lines.
	std::vector<Vertex> vertices;

	float rx = (float)x2 - x1;
	float ry =(float) y2 - y1;
	float cx = (float)x1;
	float cy = (float)y1;
	float theta = -3.14f / 2.0f;
	float dtheta = 4.0f * -3.14f / points;

	for (int i = 0; i < points; i++)
	{
		vertices.push_back(Vertex((long)(cx + rx * cos(theta)), (long)(cy + ry * sin(theta))));
		theta += dtheta;
	}

	DrawPolygons(hdc, vertices, filled, outline, fill, outlinePixels, true);
}