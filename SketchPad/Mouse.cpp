#include "stdafx.h"

Mouse::Mouse(void)
{
	InitializeMouse(0,0);
}

Mouse::Mouse(long a, long b)
{
	InitializeMouse(a,b);
}

Mouse::Mouse(const Mouse& mouse)
{
	Copy(mouse);
}

Mouse::~Mouse(void)
{

}

bool Mouse::GetLClick(void) const
{
	return lClick;
}

bool Mouse::GetRClick(void) const
{
	return rClick;
}

void Mouse::SetLClick(bool click)
{
	lClick = click;
}

void Mouse::SetRClick(bool click)
{
	rClick = click;
}

long Mouse::GetX(void) const
{
	return x;
}

long Mouse::GetY(void) const
{
	return y;
}

void Mouse::SetX(long a)
{
	x = a;
}

void Mouse::SetY(long a)
{
	y = a;
}

long Mouse::GetMemX(void) const
{
	return mX;
}

long Mouse::GetMemY(void) const
{
	return mY;
}

void Mouse::SetMemX(long a)
{
	mX = a;
}

void Mouse::SetMemY(long a)
{
	mY = a;
}

Mouse& Mouse::operator= (const Mouse &other)
{
	Copy(other);
	return *this;
}

void Mouse::InitializeMouse(long a, long b)
{
	x = a;
	y = b;
	mX = 0;
	mY = 0;
	lClick = false;
	rClick = false;
}

void Mouse::Copy(const Mouse& other)
{
	x = other.GetX();
	y = other.GetY();
	mX = other.GetMemX();
	mY = other.GetMemY();
	lClick = other.GetLClick();
	rClick = other.GetRClick();
}