#include "SketchPad.h"
#include "stdafx.h"

LRESULT CALLBACK	MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	PaintWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK	About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
BOOL				CreateNewDocument(HINSTANCE hInstance);
void				CreateUI(HWND &hWnd, LPARAM &lParam);
void				ResizeUI(HWND &hWnd);
void				SaveSketchPad(HWND &hWnd);
void				LoadSketchPad(HWND &hWnd);
void				ShapesProc(HWND &hWnd, LPARAM &lParam, WPARAM &wParam);
void				CreateNewPad(HWND &hWnd);
void				MouseCommnads(HWND &hWnd, UINT &msg, LPARAM &lParam, WPARAM &wParam, bool paintWnd);
void				KeyCommands(HWND &hWnd, LPARAM &lParam, WPARAM &wParam);
void				MainCommand(HWND &hWnd, LPARAM &lParam, WPARAM &wParam);
void				UndoCommand(void);
void				RedoCommand(void);
void				SaveDrawing(HWND &hWnd);
void				DrawWindow(HDC &hdc, HWND &hWnd);

HINSTANCE hInst;
HWND hWndMainFrame  = NULL;
HWND hWndChildFrame = NULL;
HWND toolBoxGroup, lineTool, rectTool, 
	ellipTool, fillTool, filledCheckbox,
	mainColourButton, secondaryColourButton, 
	eraserTool, polylinesTool, freehandTool,
	shapesTool, shapesComboBox, mainColourText,
	secondColourText, lineWidthEB, lineWidthText,
	lineWidthSet, saveBtn, loadBtn, undoBtn, redoBtn,
	newPadButton;

const char MainClassName[]  = "MainWindow";
const char ChildClassName[] = "PaintWindow";
const int StartChildrenNo = 994;
const int sideUI = 250;

Mouse mouse;
RECT clientWindow;
RECT paintWindow;
RECT polyStart;
HDC memoryBuffer;
HBITMAP memoryBitmap;
COLORREF mainColour = RGB(50,50,50);
COLORREF secondaryColour = RGB(255,255,255);
short pixelWidth = 1;
short shapeNo = 0;
bool loadComplete = false;
bool saveScreen = true;
bool fillShapes = false;
bool drawingPoly = false;
bool savePoly = false;
std::vector <Vertex> vertices;
std::vector <HBITMAP> savedImages;
std::vector <HBITMAP> redoFunction;
Modes mode = DrawLines;
Shapes shape = FiveStar;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int show)
{
	WNDCLASSEX WndClsEx;
	MSG Msg;

	WndClsEx.cbSize			= sizeof(WNDCLASSEX);
	WndClsEx.style			= 0;
	WndClsEx.lpfnWndProc	= MainWndProc;
	WndClsEx.cbClsExtra		= 0;
	WndClsEx.cbWndExtra		= 0;
	WndClsEx.hInstance		= hInstance;
	WndClsEx.hIcon			= NULL;
	WndClsEx.hCursor		= LoadCursor(NULL, IDC_ARROW);
	WndClsEx.hbrBackground  = (HBRUSH)COLOR_BACKGROUND + 1;
	WndClsEx.lpszMenuName   = MAKEINTRESOURCE(IDR_MAIN_MENU);
	WndClsEx.lpszClassName  = MainClassName;
	WndClsEx.hIconSm		= NULL;
	
	hInst = hInstance;

	if(!RegisterClassEx(&WndClsEx))
	{
		MessageBox(NULL, "SketchPad Failed to Start!", "Error", MB_OK);
		return 0;
	}

	if(!CreateNewDocument(hInstance))
	{
		return 0;
	}

	hWndMainFrame = CreateWindowEx(0L, MainClassName, "SketchPad", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL,  hInstance, NULL);

	if(hWndMainFrame == NULL)
	{
		MessageBox(NULL, "SketchPad Failed to Start!", "Error", MB_OK);
		return 0;
	}

	ShowWindow(hWndMainFrame, show);
	UpdateWindow(hWndMainFrame);

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDR_MAIN_MENU));

	while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		if (!TranslateMDISysAccel(hWndChildFrame, &Msg) && !TranslateAccelerator(Msg.hwnd, hAccelTable, &Msg))
		{
			TranslateMessage(&Msg);
			DispatchMessage(&Msg);
		}
	}
	return 0;
}

BOOL CreateNewDocument(HINSTANCE hInstance)
{
	WNDCLASSEX WndClsEx;

	WndClsEx.cbSize			= sizeof(WNDCLASSEX);
	WndClsEx.style			= CS_HREDRAW | CS_VREDRAW;
	WndClsEx.lpfnWndProc	= PaintWndProc;
	WndClsEx.cbClsExtra		= 0;
	WndClsEx.cbWndExtra		= 0;
	WndClsEx.hInstance		= hInstance;
	WndClsEx.hIcon			= NULL;
	WndClsEx.hCursor		= LoadCursor(NULL, IDC_CROSS);
	WndClsEx.hbrBackground  = (HBRUSH)(COLOR_BTNFACE);
	WndClsEx.lpszMenuName   = NULL;
	WndClsEx.lpszClassName  = ChildClassName;
	WndClsEx.hIconSm		= NULL;

	if(!RegisterClassEx(&WndClsEx))
	{
		MessageBox(NULL,"SketchPad failed to Register a Paint Area!","Error", MB_OK);
		return FALSE;
	}

	return TRUE;
}

HWND CreateNewMDIChild(HWND hMDIClient)
{
	MDICREATESTRUCT mcs;
	HWND NewWnd;

	mcs.szTitle = "Paint Area";
	mcs.szClass = ChildClassName;
	mcs.hOwner  = GetModuleHandle(NULL);
	mcs.x = mcs.cx = CW_USEDEFAULT;
	mcs.y = mcs.cy = CW_USEDEFAULT;
	mcs.style = MDIS_ALLCHILDSTYLES | WS_MAXIMIZE;;

	NewWnd = (HWND)SendMessage(hMDIClient, WM_MDICREATE, 0, (LONG)&mcs);

	if(!NewWnd)
		MessageBox(NULL,"SketchPad Failed to Create a Paint Area!","Error", MB_OK);

	return NewWnd;
}

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	MouseCommnads(hWnd, msg, lParam, wParam, false);
	switch(msg)
	{
		case WM_CREATE:
			{
				CreateUI(hWnd, lParam);
				CLIENTCREATESTRUCT ccs;
				GetClientRect(hWnd,&clientWindow);
				clientWindow.right -= sideUI;
				ccs.hWindowMenu  = GetSubMenu(GetMenu(hWnd), 2);
				ccs.idFirstChild = StartChildrenNo;
				hWndChildFrame = CreateWindowEx(WS_EX_CLIENTEDGE,"MDICLIENT", NULL,  WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL | WS_VISIBLE,
											   CW_USEDEFAULT,
											   CW_USEDEFAULT,
											   680,
											   460,
											   hWnd,
											   (HMENU)IDM_FILE_NEW,
											   GetModuleHandle(NULL),
											   (LPVOID)&ccs);

				if(hWndChildFrame == NULL)
					MessageBox(hWnd, "Could not create MDI client.", "Error", MB_OK | MB_ICONERROR);
			}
			CreateNewMDIChild(hWndChildFrame);
			break;
		case WM_KEYDOWN:
				KeyCommands(hWnd, lParam, wParam);
			break;
		case WM_SIZING:
			{
				RECT client = *((RECT *)lParam);

				if (client.right - client.left < 750)
					((RECT *)lParam)->right = client.left + 750;

				if (client.bottom - client.top < 525)
					((RECT *)lParam)->bottom = client.top + 525;
			}
			break;
		case WM_SIZE:
			{
				GetClientRect(hWnd,&clientWindow);
				clientWindow.right-= sideUI;
				HWND hWndMDI = GetDlgItem(hWnd, IDM_FILE_NEW);
				SetWindowPos(hWndMDI, NULL, 0, 0, clientWindow.right, clientWindow.bottom, SWP_NOZORDER);
				ResizeUI(hWnd);
			}
			break;
		case WM_CLOSE:
				DestroyWindow(hWnd);
			break;
		case WM_DESTROY:
			{
				SelectObject(memoryBuffer,memoryBitmap);
				DeleteObject(memoryBitmap);
				DeleteDC(memoryBuffer);
				PostQuitMessage(0);
			}
			break;
		case WM_COMMAND:
			{
				MainCommand(hWnd, lParam, wParam);
				ShapesProc(hWnd, lParam, wParam);
			}
			break;
		default:
			return DefFrameProc(hWnd, hWndChildFrame, msg, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK PaintWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	MouseCommnads(hWnd, msg, lParam, wParam, true);
	switch(msg)
	{
		case WM_SYSCOMMAND:
			// Deactivates MDI functionality as I didn't require it.
			break;
		case WM_CREATE:
			{
				hdc = GetDC(hWnd);
				GetClientRect(hWnd,&paintWindow);
				memoryBuffer = CreateCompatibleDC(NULL);
				memoryBitmap = CreateCompatibleBitmap(hdc, paintWindow.right, paintWindow.bottom);
				SelectObject(memoryBuffer, memoryBitmap);
				ReleaseDC(hWnd,hdc);
			}
			break;
		case WM_PAINT:
			{
				hdc = BeginPaint(hWnd, &ps);
				HBRUSH brush = CreateSolidBrush(secondaryColour);
				HGDIOBJ oldBrush = SelectObject(memoryBuffer, brush);
				BitBlt(memoryBuffer,0,0,paintWindow.right, paintWindow.bottom, memoryBuffer,0,0,PATCOPY);
				SelectObject(memoryBuffer, oldBrush);
				DeleteObject(brush);	
				DeleteObject(oldBrush);
				DrawWindow(memoryBuffer, hWnd);
				BitBlt(hdc, paintWindow.left, paintWindow.top, paintWindow.right, paintWindow.bottom, memoryBuffer,0,0,SRCCOPY);
				EndPaint(hWnd, &ps);
			}
			break;
		case WM_KEYDOWN:
				KeyCommands(hWnd, lParam, wParam);
			break;
		case WM_ERASEBKGND:
			break;
		case WM_SIZE:
			{
				hdc = GetDC(hWnd);
				GetClientRect(hWnd, &paintWindow);
				memoryBitmap = CreateCompatibleBitmap(hdc, paintWindow.right, paintWindow.bottom);
				SelectObject(memoryBuffer, memoryBitmap);
				ReleaseDC(hWnd,hdc);
			}
			return DefMDIChildProc(hWnd, msg, wParam, lParam);
		default:
			return DefMDIChildProc(hWnd, msg, wParam, lParam);
	}
	
	return 0;
}
 
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void CreateUI(HWND &hWnd, LPARAM &lParam)
{
	toolBoxGroup = CreateWindowW(L"button", L"Toolbox",	WS_VISIBLE | WS_CHILD  |BS_GROUPBOX  , 0, 0, 0, 0, hWnd, (HMENU)IDC_ToolBox, NULL, NULL);
	freehandTool = CreateWindowW(L"button", L"Freehand", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDC_FreehandButton, NULL, NULL);
	eraserTool = CreateWindowW(L"button", L"Eraser", WS_VISIBLE | WS_CHILD , 0, 0, 0, 0, hWnd, (HMENU)IDC_EraserButton, NULL, NULL);
	rectTool = CreateWindowW(L"button", L"Rectangles",	WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDC_RectangleButton, NULL, NULL);
	lineTool = CreateWindowW(L"button", L"Lines",	WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDC_LineButton, NULL, NULL);
	ellipTool = CreateWindowW(L"button", L"Ellipses",	WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDC_EllipsesButton, NULL, NULL);
	polylinesTool = CreateWindowW(L"button", L"Polylines", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDC_PolylineButton, NULL, NULL);
	fillTool = CreateWindowW(L"button", L"Fill", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_FillButton, NULL, NULL);
	filledCheckbox = CreateWindowW(L"button", L"Fill Shapes",WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 0, 0, 0, 0, hWnd, (HMENU) IDC_FillCheckbox, NULL, NULL);
	shapesTool = CreateWindowW(L"button", L"Shapes", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_ShapesButton, NULL, NULL);
	shapesComboBox = CreateWindowW(L"combobox", L"Shapes",	WS_VISIBLE | WS_CHILD | WS_VSCROLL | CBS_DROPDOWNLIST | WS_TABSTOP ,0, 0, 0, 0, hWnd, (HMENU)IDC_ShapesComboBox, NULL, NULL);
	mainColourButton = CreateWindowW(L"button", L"", WS_VISIBLE | WS_CHILD | BS_BITMAP, 0, 0, 0, 0, hWnd, (HMENU)IDC_MainColourButton, NULL, NULL);
	secondaryColourButton = CreateWindowW(L"button", L"",	WS_VISIBLE | WS_CHILD| BS_BITMAP, 0, 0, 0, 0, hWnd, (HMENU)IDC_SecondColourButton, NULL, NULL);
	mainColourText = CreateWindowW(L"STATIC", L"Fore Colour", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)1, NULL, NULL);
	secondColourText = CreateWindowW(L"STATIC", L"Back Colour", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)1, NULL, NULL);
	lineWidthEB = CreateWindowW(L"Edit", L"1", WS_VISIBLE | WS_CHILD | WS_BORDER | ES_NUMBER | ES_CENTER, 0, 0, 0, 0, hWnd, (HMENU)IDC_LineWidthEdit, NULL, NULL);
	lineWidthText = CreateWindowW(L"STATIC", L"Line Width",	WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)1, NULL, NULL);
	lineWidthSet = CreateWindowW(L"button", L"Set", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDC_LineWidthSet, NULL, NULL);
	saveBtn = CreateWindowW(L"button", L"Save", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDM_FILE_SAVE, NULL, NULL);
	loadBtn = CreateWindowW(L"button", L"Load", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDM_FILE_LOAD, NULL, NULL);
	undoBtn = CreateWindowW(L"button", L"Undo", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)ID_EDIT_UNDO, NULL, NULL); 
	redoBtn = CreateWindowW(L"button", L"Redo", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)ID_EDIT_REDO, NULL, NULL); 
	newPadButton = CreateWindowW(L"button", L"New", WS_VISIBLE | WS_CHILD  , 0, 0, 0, 0, hWnd, (HMENU)IDC_NewFile, NULL, NULL); 

	// Making all of the text the same defualt font.
	HFONT defaultFont;
	defaultFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
	SendMessage(toolBoxGroup, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(freehandTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(eraserTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(rectTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lineTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(polylinesTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(fillTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(filledCheckbox, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(shapesTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(mainColourText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(secondColourText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lineWidthEB, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(saveBtn, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(loadBtn, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(undoBtn, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(redoBtn, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(newPadButton, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lineWidthText, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(lineWidthSet, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(ellipTool, WM_SETFONT, WPARAM (defaultFont), TRUE);
	SendMessage(shapesComboBox, WM_SETFONT, WPARAM (defaultFont), TRUE);	

	//Initialize components
	CheckDlgButton(hWnd, IDC_FillCheckbox, BST_UNCHECKED);
	for(int i = 0; i < 3; i++)
		SendMessage(shapesComboBox, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>((LPCTSTR)ShapeStrings[i]));
	SendMessage(shapesComboBox, CB_SETCURSEL, 0, NULL);

	Utility::ColourButton(mainColour, mainColourButton);
	Utility::ColourButton(secondaryColour, secondaryColourButton);
}

void ResizeUI(HWND &hWnd)
{
	const int leftSideBtn = clientWindow.right + 20;
	const int rightSideBtn = clientWindow.right + 130;

	ShowWindow( toolBoxGroup, SW_HIDE ); 
	MoveWindow(toolBoxGroup, clientWindow.right + 10, 10, 230, 440, FALSE);
	MoveWindow(freehandTool, leftSideBtn, 30, 100,25, FALSE);
	MoveWindow(eraserTool, rightSideBtn, 30, 100, 25, FALSE);
	MoveWindow(rectTool, leftSideBtn, 70,100,25, FALSE);
	MoveWindow(lineTool, rightSideBtn, 70, 100,25, FALSE);
	MoveWindow(ellipTool, leftSideBtn, 110,100,25, FALSE);
	MoveWindow(polylinesTool, rightSideBtn, 110,100,25, FALSE);
	MoveWindow(fillTool, leftSideBtn, 150,100,25, FALSE);
	MoveWindow(shapesTool, leftSideBtn, 190,100,25, FALSE);
	MoveWindow(shapesComboBox, rightSideBtn, 190,100,100, FALSE);
	MoveWindow(filledCheckbox,rightSideBtn, 150, 100, 25, FALSE);
	MoveWindow(mainColourButton, leftSideBtn + 30, 220, 40, 40,  FALSE);
	MoveWindow(secondaryColourButton, rightSideBtn + 30, 220, 40, 40, FALSE);
	MoveWindow(mainColourText, leftSideBtn + 10, 260, 80, 25,  FALSE);
	MoveWindow(secondColourText, rightSideBtn + 10, 260, 80, 25, FALSE);
	MoveWindow(lineWidthEB, rightSideBtn, 290, 40, 25, FALSE);
	MoveWindow(lineWidthText, rightSideBtn -  90, 292, 80, 25, FALSE);
	MoveWindow(lineWidthSet, rightSideBtn + 50, 290, 50, 25, FALSE);
	MoveWindow(saveBtn, leftSideBtn, 330, 100,25, FALSE);
	MoveWindow(loadBtn, leftSideBtn, 370, 100, 25, FALSE);
	MoveWindow(undoBtn, rightSideBtn, 330,100,25, FALSE);
	MoveWindow(redoBtn, rightSideBtn, 370, 100,25, FALSE);
	MoveWindow(newPadButton, clientWindow.right + 80, 410, 100, 25, FALSE);
	ShowWindow(toolBoxGroup,SW_SHOW); 
}

void SaveSketchPad(HWND &hWnd)
{
	if(savedImages.empty())
	{
		MessageBox(hWnd, "There is nothing to save!\nGet drawing!", "SketchPad Error", MB_OK);
		return;
	}

	char fileString[MAX_PATH] = "";
	if(Utility::FileDialog(hWnd, fileString, "Save SketchPad", true))
		Utility::SaveBitmap(fileString, savedImages.back(), paintWindow.right, paintWindow.bottom);	
}

void LoadSketchPad(HWND &hWnd)
{
	HBITMAP loadBitmap;
	char fileString[MAX_PATH] = "";
	if(Utility::FileDialog(hWnd, fileString, "Load SketchPad", false))
	{
		loadBitmap =  (HBITMAP)LoadImage(NULL, fileString, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_DEFAULTCOLOR | LR_DEFAULTSIZE | LR_CREATEDIBSECTION);
		if(NULL == loadBitmap)
		{
			MessageBox(hWnd,"Load Image Failed","Error: The image was not compatible!",MB_OK);
		} 
		// Saves the newly recieved image.
		savedImages.push_back(loadBitmap);
	}
	HWND mdi = FindWindow("Paint Area","PaintWindow");
	InvalidateRect(mdi, NULL, FALSE);
}

void ShapesProc(HWND &hWnd, LPARAM &lParam, WPARAM &wParam)
{
	switch(LOWORD(wParam))
    {
    case IDC_ShapesComboBox:
        switch(HIWORD(wParam))
        {
		case CBN_SELCHANGE: 
			shapeNo = (short) SendMessage(shapesComboBox, CB_GETCURSEL, 0, 0);
			switch(shapeNo)
			{
				case 0:
					shape = FiveStar;
				break;
				case 1:
					shape = Triangle;
				break;
				case 2:
					shape = RightAngleTriangle;
				break;
			}
        break;
        }
    break;
	}
}

void CreateNewPad(HWND &hWnd)
{
	if(MessageBox(hWnd, "Do you want to create a new pad?", "Create New Pad", MB_YESNO) == IDYES)
	{
		HWND mdi = FindWindow("Paint Area","PaintWindow");
		mainColour = RGB(50,50,50);
		Utility::ColourButton(mainColour, mainColourButton);
		vertices.clear();
		savedImages.clear();
		pixelWidth = 1;
		loadComplete = false;
		saveScreen = true;
		fillShapes = false;
		drawingPoly = false;
		savePoly = false;
		mode = DrawLines;
		shape = FiveStar;
		InvalidateRect(mdi, NULL, FALSE);
	}
}

void MouseCommnads(HWND &hWnd, UINT &msg, LPARAM &lParam, WPARAM &wParam, bool paintWnd)
{
	POINT mousePoint;
	switch(msg)
	{
		case WM_MOUSEWHEEL:
				{
					if ((short) HIWORD(wParam) / 120 > 0 )
					{
						if(pixelWidth < 9999)
							pixelWidth++;
					}
					else
						if(pixelWidth > 1)
							pixelWidth--;

					SetDlgItemInt(hWnd, IDC_LineWidthEdit, (UINT)pixelWidth, FALSE);
				}
				break;
			case WM_MOUSEMOVE:
				{
					if(!paintWnd)
						break;

					GetCursorPos(&mousePoint);
					ScreenToClient(hWnd, &mousePoint);
					mouse.SetX(mousePoint.x);
					mouse.SetY(mousePoint.y);

					if(!mouse.GetLClick())
						if(!drawingPoly)
							break;

					HWND mdi = FindWindow("Paint Area","PaintWindow");
					InvalidateRect(hWnd, &paintWindow, false);
				}
				break;
			case WM_LBUTTONDOWN:
				{
					if(!paintWnd)
						break;

					if(!mouse.GetLClick())
						SetCapture(hWnd);

					GetCursorPos(&mousePoint);
					ScreenToClient(hWnd, &mousePoint);
					mouse.SetMemX(mousePoint.x);
					mouse.SetMemY(mousePoint.y);
					mouse.SetLClick(true);
					InvalidateRect(hWnd ,&paintWindow, false);
				}
				break;
			case WM_LBUTTONUP:
				{
					if(!paintWnd)
						break;

					if(mouse.GetLClick() && !drawingPoly)
						ReleaseCapture();

					mouse.SetLClick(false);

					if(!drawingPoly)
					{
						savePoly = false;
						vertices.clear();
						SaveDrawing(hWnd);
					}
					else
						savePoly = true;
				}
				break;
			case WM_RBUTTONDOWN:
				{					
					if(!paintWnd)
						break;

					mouse.SetRClick(true);
				}
				break;
			case WM_RBUTTONUP:
				{
					if(!paintWnd)
						break;

					mouse.SetRClick(false); 
				}
				break;
	}
}

void KeyCommands(HWND &hWnd, LPARAM &lParam, WPARAM &wParam)
{			
	if(wParam == 0x5A) 
	{
		//Z Key - Undo Function
		UndoCommand();
	}
	else if(wParam == 0x58) 
	{
		//X Key - Undo Function
		RedoCommand();
	}
	else if(wParam == 0x4E) 
	{
		//N Key - New Pad Function
		CreateNewPad(hWnd);
	}
	else if(wParam == 0x53) 
	{
		//S Key - Save Function
		SaveSketchPad(hWnd);
	}
	else if(wParam == 0x4C) 
	{
		//L Key - Load Function
		LoadSketchPad(hWnd);
	}
}

void MainCommand(HWND &hWnd, LPARAM &lParam, WPARAM &wParam) 
{
	switch(LOWORD(wParam))
	{
		case IDC_FillCheckbox:
			{
				if(fillShapes) 
				{
					fillShapes = false;
					CheckDlgButton(hWnd, IDC_FillCheckbox, BST_UNCHECKED);
				}
				else if(!fillShapes)
				{
					fillShapes = true;
					CheckDlgButton(hWnd, IDC_FillCheckbox, BST_CHECKED);
				}
			}
			break;
		case IDC_LineButton:
			{
				mode = DrawLines;	
				SetFocus(hWnd);
			}
			break;
		case IDC_RectangleButton:
			{
				mode = DrawRects;
				SetFocus(hWnd);
			}
			break;
		case IDC_EllipsesButton:
			{
				mode = DrawEllip;
				SetFocus(hWnd);
			}
			break;
		case IDC_FillButton:
			{
				mode = DrawFill;
				SetFocus(hWnd);
			}
			break;
		case IDC_EraserButton:
			{
				mode = DrawEraser;
				SetFocus(hWnd);
			}
			break;
		case IDC_FreehandButton:
			{
				mode = DrawFreehand;
				SetFocus(hWnd);
			}
			break;
		case IDC_PolylineButton:
			{
				mode = DrawPolylines;
				SetFocus(hWnd);
			}
			break;
		case IDC_ShapesButton:
			{
				mode = DrawShapes;
				SetFocus(hWnd);
			}
			break;
		case IDC_MainColourButton:
			{
				mainColour = Utility::ColourDialog(mainColour, hWnd);
				Utility::ColourButton(mainColour, mainColourButton);
				SetFocus(hWnd);
			}
			break;
		case IDC_SecondColourButton:
			{
				secondaryColour = Utility::ColourDialog(secondaryColour, hWnd);
				Utility::ColourButton(secondaryColour, secondaryColourButton);
				SetFocus(hWnd);
			}
			break;
		case IDC_LineWidthSet:
			{
				if(GetDlgItemInt(hWnd, IDC_LineWidthEdit, NULL, FALSE) != NULL || GetDlgItemInt(hWnd, IDC_LineWidthEdit, NULL, FALSE) != 0)
					pixelWidth = GetDlgItemInt(hWnd, IDC_LineWidthEdit, NULL, FALSE);
				else
				{
					pixelWidth = 1;
					SetDlgItemInt(hWnd, IDC_LineWidthEdit, 1, FALSE);
				}
				SetFocus(hWnd);
			}
			break;
		case IDM_FILE_NEW:
			{
				CreateNewPad(hWnd);
				SetFocus(hWnd);
			}
			break;
		case IDC_NewFile :
			{
				CreateNewPad(hWnd);
				SetFocus(hWnd);
			}
			break;
		case IDM_FILE_SAVE:
			{
				SaveSketchPad(hWnd);
				SetFocus(hWnd);
			}
			break;
		case IDM_FILE_LOAD:
			{
				LoadSketchPad(hWnd);
				SetFocus(hWnd);
			}
			break;
		case ID_EDIT_UNDO:
			{
				UndoCommand();
				SetFocus(hWnd);
			}
			break;
		case ID_EDIT_REDO:
			{
				RedoCommand();
				SetFocus(hWnd);
			}
			break;
		case ID_HELP_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
		case IDM_FILE_EXIT:
				PostMessage(hWnd, WM_CLOSE, 0, 0);
			break;
		default:
			{
				if(LOWORD(wParam) >= StartChildrenNo)
					DefFrameProc(hWnd, hWndChildFrame, WM_COMMAND, wParam, lParam);
				else 
				{
					HWND hWndCurrent = (HWND)SendMessage(hWndChildFrame, WM_MDIGETACTIVE,0,0);
					if(hWndCurrent)
						SendMessage(hWndCurrent, WM_COMMAND, wParam, lParam);
			}
		}
	}
}

void UndoCommand(void)
{
	if(savedImages.size() > 1)
	{
		HWND mdi = FindWindow("Paint Area","PaintWindow");
		redoFunction.push_back(savedImages.back());
		savedImages.pop_back();
		InvalidateRect(mdi, NULL, false);
	}
}

void RedoCommand(void)
{
	if(!redoFunction.empty())
	{
		HWND mdi = FindWindow("Paint Area","PaintWindow");
		savedImages.push_back(redoFunction.back());
		redoFunction.pop_back();
		InvalidateRect(mdi, NULL, false);
	}
}

void SaveDrawing(HWND &hWnd)
{
	//Saved drawings is based upon the idea of Double buffering.
	//I just extended it further by saving the returned image into a vector of HBITMAPS.
	HDC main = GetDC(hWnd);
	HDC storeDC = CreateCompatibleDC(main);
	HBITMAP PaintBitmap = CreateCompatibleBitmap(main, paintWindow.right,paintWindow.bottom);
	HBITMAP OldBitmap = (HBITMAP)SelectObject(storeDC, PaintBitmap);	
	BitBlt(storeDC, 0, 0, paintWindow.right, paintWindow.bottom, main, 0, 0, SRCCOPY);
	//Saving the returned image here.
	PaintBitmap = (HBITMAP)SelectObject(storeDC, OldBitmap);
	DeleteDC(storeDC);
	DeleteDC(main);
	savedImages.push_back(PaintBitmap);
	DeleteObject(OldBitmap);
}

void DrawWindow(HDC &hdc, HWND &hWnd)
{
	if(!savedImages.empty())
	{
		//This statement puts transfers the most recent paintings into the buffer.
		HDC storeDC = CreateCompatibleDC(NULL);
		HBITMAP OldBitmap = (HBITMAP)SelectObject(storeDC, savedImages.back());
		BitBlt(hdc, 0, 0, clientWindow.right, clientWindow.bottom, storeDC, 0, 0, SRCCOPY);
		SelectObject(storeDC, OldBitmap);
		DeleteDC(storeDC);
		DeleteObject(OldBitmap);
	}
	else
	{
		//Stores the first image on start up.
		if (loadComplete && saveScreen)
		{
			saveScreen = false;
			SaveDrawing(hWnd);
		}
		else
			loadComplete = true;
	}
	
	// This class call handles all of the drawing procedures.
	ApplicationEngine app(hdc, mouse, mainColour, secondaryColour, pixelWidth, fillShapes, drawingPoly, savePoly, polyStart, vertices, mode, shape);
	
	// Returning the update variables from the application engine class.
	vertices = app.ReturnStoredVertices();
	drawingPoly = app.DrawingPoly();
	savePoly = app.SavingPoly();
	polyStart = app.PolylineStart();

	// If the app engine requires the drawings to be saved.
	if(app.SaveDrawing())
		SaveDrawing(hWnd);
}