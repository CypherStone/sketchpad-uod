#include "Utility.h"
#include "stdafx.h"

bool Utility::FileDialog(HWND &hWnd, char *fileString, char *title, bool saving)
{
	OPENFILENAME fileDialog;
	SecureZeroMemory(&fileDialog, sizeof(fileDialog));
	fileDialog.lStructSize= sizeof(fileDialog);
	fileDialog.hwndOwner = hWnd;
	fileDialog.lpstrTitle = title;
	fileDialog.lpstrFilter = "Bitmap (*.BMP)\0*.BMP\0";
	fileDialog.lpstrFile = fileString;
	fileDialog.nMaxFile = MAX_PATH;
	fileDialog.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST;
	fileDialog.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY |OFN_OVERWRITEPROMPT | OFN_NOREADONLYRETURN;
	fileDialog.lpstrDefExt = "bmp";

	if(saving)
	{
		if(GetSaveFileName(&fileDialog))
		{
			return true;
		}
	}
	else
	{
		if(GetOpenFileName(&fileDialog))
		{
			return true;
		}
	}
	return false;
}

void Utility::SaveBitmap(char *fileString, HBITMAP &hBitmap, const long &imageWidth, const long &imageHeight)
{
	HANDLE hFile = CreateFile(fileString, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if(hFile != INVALID_HANDLE_VALUE)
	{
		BITMAP bitmap;
		GetObject(hBitmap, sizeof(BITMAP), &bitmap);
		int size = (imageWidth) * (imageHeight) * bitmap.bmBitsPixel / 8;
		BYTE *lpBits = new BYTE[size];
		GetBitmapBits(hBitmap, size, lpBits);
		CloseHandle(hFile);

		BITMAPINFO Bmi;
		memset(&Bmi, 0, sizeof(BITMAPINFO));
		Bmi.bmiHeader.biSize		= sizeof(BITMAPINFOHEADER);
		Bmi.bmiHeader.biWidth		= bitmap.bmWidth;
		Bmi.bmiHeader.biHeight		= bitmap.bmHeight;
		Bmi.bmiHeader.biPlanes		= 1;
		Bmi.bmiHeader.biBitCount	= bitmap.bmBitsPixel; 
		Bmi.bmiHeader.biCompression	= BI_RGB;
		Bmi.bmiHeader.biSizeImage	= bitmap.bmHeight * bitmap.bmWidth * bitmap.bmBitsPixel / 8; 

		FILE* image;
		fopen_s (&image, fileString, "wb");
		if(image==0)
		{
			return;
		}

		int h = abs(Bmi.bmiHeader.biHeight);
		int w = abs(Bmi.bmiHeader.biWidth);
		Bmi.bmiHeader.biHeight=-h;
		Bmi.bmiHeader.biWidth=w;
		int sz = Bmi.bmiHeader.biSizeImage;

		BITMAPFILEHEADER bfh;
		bfh.bfType=('M'<<8)+'B'; 
		bfh.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER); 
		bfh.bfSize=sz+bfh.bfOffBits; 
		bfh.bfReserved1=0; 
		bfh.bfReserved2=0; 

		fwrite(&bfh, sizeof(bfh), 1, image);
		fwrite(&Bmi. bmiHeader, sizeof(BITMAPINFOHEADER), 1, image);
		fwrite(lpBits, sz, 1, image);
		fclose(image);
			
		delete []lpBits;
	}
}

COLORREF Utility::ColourDialog(const COLORREF colour, HWND &hWnd)
{
	CHOOSECOLOR colourDialog;
	static COLORREF acrCustClr[16];
	SecureZeroMemory(&colourDialog, sizeof(colourDialog));
	colourDialog.lStructSize = sizeof(colourDialog);
	colourDialog.hwndOwner = hWnd;
	colourDialog.lpCustColors = (LPDWORD) acrCustClr;
	colourDialog.rgbResult = colour;
	colourDialog.Flags = CC_FULLOPEN | CC_RGBINIT;
 
	if (ChooseColor(&colourDialog) == TRUE) 
	{
		return colourDialog.rgbResult; 
	}
	else
	{
		return colour;
	}
}

void Utility::ColourButton(const COLORREF colour, HWND &button)
{
	//Colouring the button creates a small bitmap and transfers it onto a button.
	HDC hdc = GetDC(button);
	HDC hdcTemp = CreateCompatibleDC(hdc);
	HBITMAP main = CreateCompatibleBitmap(hdc, 30,30);
	HBRUSH brush = CreateSolidBrush(colour);
	HBRUSH oldBrush = (HBRUSH)SelectObject(hdcTemp, brush);
	HBITMAP oldBit = (HBITMAP)SelectObject(hdcTemp, main);
	BitBlt(hdcTemp, 2, 2, 26, 26, hdcTemp, 0, 0, PATCOPY);
	SelectObject(hdc,oldBrush);
	main = (HBITMAP)SelectObject(hdcTemp,oldBit);
	SendMessage(button, BM_SETIMAGE , (WPARAM)IMAGE_BITMAP,(LPARAM)main);
	DeleteObject(brush);
	DeleteObject(oldBit);
	DeleteObject(oldBrush);
	ReleaseDC(button, hdc);
}

