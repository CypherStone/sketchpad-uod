#include "stdafx.h"

ApplicationEngine::ApplicationEngine(void)
{
	// Do nothing.
}

ApplicationEngine::ApplicationEngine(HDC hdc, Mouse mouse, COLORREF mainColour, 
	COLORREF secondColour, short lineWidth, bool fillShapes, bool drawPoly,
	bool savePoly, RECT polylineStart, std::vector <Vertex> vertices, Modes mode, Shapes shape)
{
	// This is setting all of the required variables to this class.
	_hdc = hdc;
	_mouse = mouse;
	_mainColour = mainColour;
	_secondColour = secondColour;
	_lineWidth = lineWidth;
	_fillShapes = fillShapes;
	_drawingPoly = drawPoly;
	_savePoly = savePoly;
	_polylineStart = polylineStart;
	_vertices = vertices;
	_mode = mode;
	_shape = shape;

	_toSave = false;

	SketchPadMode();
}

ApplicationEngine::~ApplicationEngine(void)
{
	// Do nothing.
}

void ApplicationEngine::SketchPadMode(void)
{
	// Handles what is going to be drawn from the user input.
	if(_mouse.GetLClick() || _drawingPoly)
	{
		long x1 = _mouse.GetMemX();
		long y1 = _mouse.GetMemY();
		long x2 = _mouse.GetX();
		long y2 = _mouse.GetY();

		switch (_mode)
		{
			case DrawLines:
					Drawing::DrawLine(_hdc, x1, y1, x2, y2, _mainColour, _lineWidth, Scanline(), false);
				break;
			case DrawRects:
					Drawing::DrawRectangles(_hdc, x1, y1, x2, y2, _fillShapes, _mainColour, _secondColour, _lineWidth);
				break;
			case DrawEllip:
				{
					RECT rect;
					rect.left = x1;
					rect.top = y1;
					rect.right = x2;
					rect.bottom = y2;
					Drawing::DrawEllipses(_hdc, rect, _fillShapes, _mainColour, _secondColour, _lineWidth);
				}
				break;
			case DrawFill:
				{
					SetCursor(LoadCursor(NULL,IDC_WAIT));
					Drawing::FloodFill(_hdc, x1, y1, _mainColour, GetPixel(_hdc, x1, y1), _lineWidth);
					SetCursor(LoadCursor(NULL,IDC_ARROW));
				}
				break;
			case DrawEraser:
				{
					_vertices.push_back(Vertex(x2,y2));
					for(int limit = 0; limit < (int) _vertices.size(); limit++)
					{
						Drawing::DrawPoint(_hdc, _vertices[limit], _secondColour, _lineWidth);
						// This if statement saves the current state and resets it.
						// This is improves the undo function and possible perfomance issues.
						if(limit > 500)
						{
							_vertices.clear();
							_toSave = true;
						}
					}
				}
				break;
			case DrawPolylines:
				{
					if(_drawingPoly) 
					{
						if ((x2 >= _polylineStart.left && x2 <= _polylineStart.right) && (y2 >= _polylineStart.top && y2 <= _polylineStart.bottom) && _mouse.GetLClick() && _vertices.size() > 1)
						{
							_drawingPoly = false;
							Drawing::DrawPolygons(_hdc, _vertices, _fillShapes, _mainColour, _secondColour, _lineWidth, true);
							_vertices.clear();
						}
						else
						{
							if(_savePoly)
								_vertices.push_back(Vertex(x1, y1));

							Drawing::DrawRectangles(_hdc, _polylineStart.left, _polylineStart.top, _polylineStart.right, _polylineStart.bottom,  false, COLORREF(RGB(000,000,000)), NULL, 1);
							Drawing::DrawLine(_hdc,x1, y1, x2, y2, _mainColour, _lineWidth, Scanline(), false);
							Drawing::DrawPolygons(_hdc, _vertices, _fillShapes, _mainColour, _secondColour, _lineWidth, false);
						}
					}
					else if(!_drawingPoly)
					{
						_drawingPoly = true;
						_polylineStart.left = x1 - 10;
						_polylineStart.top = y1 - 10;
						_polylineStart.right = x1 + 10;
						_polylineStart.bottom = y1 + 10;
						Drawing::DrawRectangles(_hdc, _polylineStart.left, _polylineStart.top, _polylineStart.right, _polylineStart.bottom, false, COLORREF(RGB(000,000,000)),NULL, 1);
					}				
				}
				break;
			case DrawFreehand:
				{
					_vertices.push_back(Vertex(x2, y2));
					for(int limit = 0; limit < (int) _vertices.size(); limit++)
					{
						if(_lineWidth < 2)
							Drawing::DrawPoint(_hdc, _vertices[limit], _mainColour, 1);
						else
						{
							RECT rect;
							rect.left = _vertices[limit].GetX();
							rect.top = _vertices[limit].GetY();
							rect.right = _vertices[limit].GetX() + _lineWidth;
							rect.bottom = _vertices[limit].GetY() + _lineWidth;
							Drawing::DrawEllipses(_hdc, rect, true, _mainColour, _mainColour, 1);
						}

						if(limit > 250)
						{
							_vertices.clear();
							_toSave = true;
						}
					}
				}
				break;
			case DrawShapes:
				{
					switch(_shape)
					{
						case FiveStar:
								Drawing::DrawStar(_hdc, x1, y1, x2, y2, 5, _fillShapes, _mainColour, _secondColour, _lineWidth);
							break;
						case Triangle:
							{
								_vertices.push_back(Vertex(x1 + (x2 - x1), y1 + (y2 - y1)));
								_vertices.push_back(Vertex(x1, y1));
								_vertices.push_back(Vertex(x1 + (2* (x2 - x1)), y1));
								Drawing::DrawPolygons(_hdc, _vertices, _fillShapes, _mainColour, _secondColour, _lineWidth, true);
								_vertices.clear();
							}
							break;
						case RightAngleTriangle:
							{
								_vertices.push_back(Vertex(x1 + (x2 - x1), y1 + (y2 - y1)));
								_vertices.push_back(Vertex(x1, y1));
								_vertices.push_back(Vertex(x1, y1 + (y2 - y1)));
								Drawing::DrawPolygons(_hdc, _vertices, _fillShapes, _mainColour, _secondColour, _lineWidth, true);
								_vertices.clear();
							}
							break;
					}
				}
			break;
		}
	}	

}

bool ApplicationEngine::SaveDrawing(void)
{
	return _toSave;
}

bool ApplicationEngine::DrawingPoly(void)
{
	return _drawingPoly;
}

bool ApplicationEngine::SavingPoly(void)
{
	return _savePoly;
}

RECT ApplicationEngine::PolylineStart(void)
{
	return _polylineStart;
}

std::vector<Vertex> ApplicationEngine::ReturnStoredVertices(void)
{
	return _vertices;
}