#pragma once
#include "stdafx.h"

class Scanline
{
	public:
		Scanline(void);
		Scanline(const std::vector<Vertex> vertex);
		Scanline(const Scanline &scan);
		~Scanline(void);

		void AddScanline(int y, int x);

		int GetScanline(int y, int x);
		int GetScanlineMinY(void);
		int GetScanlineMinX(void);
		int GetScanlineMaxY(void);
		int GetScanlineMaxX(void);

		std::vector<std::vector<int>> _scanline;
		void Sort(void);
	private:

		int _yMax;
		int _yMin;
		int _xMax;
		int _xMin;
		void Copy(const Scanline &scan);
};