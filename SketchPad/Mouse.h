#pragma once
#include "stdafx.h"

class Mouse
{
	public:
		Mouse(void);
		Mouse(long,long);
		Mouse(const Mouse& mouse);
		~Mouse();
	
		bool GetLClick(void) const;
		bool GetRClick(void) const;
		void SetLClick(bool);
		void SetRClick(bool);

		long GetX(void) const;
		long GetY(void) const;
		void SetX(long);
		void SetY(long);
		
		long GetMemX(void) const;
		long GetMemY(void) const;
		void SetMemX(long);
		void SetMemY(long);

		Mouse& operator= (const Mouse &other);

	private:
		long x,y;
		long mX,mY;
		bool lClick, rClick;

		void InitializeMouse(long,long);
		void Copy(const Mouse&);
};