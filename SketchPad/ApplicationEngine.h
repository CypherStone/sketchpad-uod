#pragma once
#include "stdafx.h"
#include "Mouse.h"

enum Modes
{
	DrawRects,
	DrawLines,
	DrawPixels,
	DrawEllip,
	DrawFill,
	DrawEraser,
	DrawPolylines,
	DrawFreehand,
	DrawShapes,
	PickPixel
};

enum Shapes
{
	FiveStar,
	Triangle,
	RightAngleTriangle,
};

class ApplicationEngine
{
	public:
		ApplicationEngine(void);
		ApplicationEngine(HDC hdc, class Mouse mouse, COLORREF mainColour, COLORREF secondColour, 
			short lineWidth, bool fillShapes, bool drawPoly, bool savePoly, RECT polylineStart, 
			std::vector <Vertex> vertices, Modes mode,	Shapes shape);
		~ApplicationEngine(void);
		bool SaveDrawing(void);
		bool DrawingPoly(void);
		bool SavingPoly(void);
		RECT ApplicationEngine::PolylineStart(void);
		std::vector<Vertex> ReturnStoredVertices(void);

	private:
		void SketchPadMode(void);

		HDC _hdc;
		Mouse _mouse;
		COLORREF _mainColour;
		COLORREF _secondColour;
		short _lineWidth;
		bool _fillShapes;
		bool _drawingPoly;
		bool _savePoly;
		RECT _polylineStart;
		std::vector <Vertex> _vertices;
		Modes _mode;
		Shapes _shape;
		bool _toSave;
};