//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDR_MAIN_MENU                   101
#define IDD_ABOUTBOX                    103
#define IDM_FILE_NEW                    50000
#define IDM_FILE_SAVE					50001
#define IDM_FILE_LOAD					50002
#define ID_HELP_ABOUT                   40002
#define ID_EDIT_UNDO                    40003
#define ID_EDIT_REDO                    40004
#define IDM_FILE_EXIT                   40006
#define IDC_ToolBox                     40008
#define IDC_LineButton                  40009
#define IDC_RectangleButton             40010
#define IDC_EllipsesButton              40011
#define IDC_FillButton                  40012
#define IDC_FillCheckbox                40013
#define IDC_MainColourButton            40018
#define IDC_SecondColourButton          40019
#define IDC_EraserButton                40020
#define IDC_PolylineButton              40021
#define IDC_FreehandButton              40022
#define IDC_ShapesButton	            40023
#define IDC_ShapesComboBox				40024
#define IDC_LineWidthEdit				40025
#define IDC_LineWidthSet				40026
#define IDC_NewFile						40027
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
