#pragma once
#include "stdafx.h"

struct Lines
{
	Vertex start;
	Vertex end;
	COLORREF colour;
	short size;
	short layer;
};

struct Rectangles
{
	RECT rect;
	COLORREF colourOutline;
	COLORREF colourFill;
	bool filled;
	short size;
	short layer;
};

struct Ellipses
{
	RECT rect;
	COLORREF colourOutline;
	COLORREF colourFill;
	short size;
	short layer;
};

