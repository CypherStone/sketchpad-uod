#pragma once

class Vertex
{
	public:
		Vertex(void);
		Vertex(long x, long y);
		Vertex(const Vertex& v);
		~Vertex(void);

		long GetX(void) const;
		void SetX(long x);
		long GetY(void) const;
		void SetY(long y);

		Vertex& operator= (const Vertex &rhs);
		const Vertex operator+(const Vertex &other) const;
		const Vertex operator-(const Vertex &other) const;

	private:
		long _x;
		long _y;
		

		void Init(long x, long y);
		void Copy(const Vertex& v);
		
};
